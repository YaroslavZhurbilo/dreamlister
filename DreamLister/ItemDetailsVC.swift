//
//  ItemDetailsVC.swift
//  DreamLister
//
//  Created by Yaroslav Zhurbilo on 08.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit
import CoreData

class ItemDetailsVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSFetchedResultsControllerDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleField: CustomTextField!
    @IBOutlet weak var priceField: CustomTextField!
    @IBOutlet weak var detailsField: CustomTextField!
    @IBOutlet weak var storeLabel: UILabel!
    @IBOutlet weak var storePicker: UIPickerView!
    
    // var controller: NSFetchedResultsController<Store>!
    
    var stores = [Store]()
    var itemToEdit: Item?
    var imagePicker: UIImagePickerController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        storePicker.dataSource = self
        storePicker.delegate   = self
        
        imagePicker = UIImagePickerController()
        imagePicker.delegate   = self
        
        if let topItem = self.navigationController?.navigationBar.topItem {
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
//        generateTestDataForStore()
        
        getStores()
        
        if itemToEdit != nil {
            loadItemData()
        }
        
    }
    
    func generateTestDataForStore() {
        let store0 = Store(context: context)
        store0.name = "?"
        
        let store1 = Store(context: context)
        store1.name = "Amazon"
        
        let store2 = Store(context: context)
        store2.name = "Tesla Dealership"
        
        let store3 = Store(context: context)
        store3.name = "Rozetka"
        
        let store4 = Store(context: context)
        store4.name = "Эпицентр"
        
        ad.saveContext()
    }
    
    func getStores() {
        let fetchRequest: NSFetchRequest<Store> = Store.fetchRequest()
        
        do {
            self.stores = try context.fetch(fetchRequest)
            self.stores.sort(by: { _store1, _store2 in
                if let value1 = _store1.name, let value2 = _store2.name {
                    return value1 < value2
                } else {
                    return false
                }
            })
            self.storePicker.reloadAllComponents()
        } catch {
            print(error)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
//        if let sections = controller.sections {
//            return sections.count
//        }
        
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
//        if let sections = controller.sections {
//            return sections[component].numberOfObjects
//        }
        
        return stores.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if row == 0 {
            return "Select Store"
        } else {
            return stores[row].name
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //
    }
    @IBAction func saveItemBtn(_ sender: UIButton) {
        
        guard let title = titleField.text, title != "" else {
            print("no title")
            return
        }
        guard let value = priceField.text,
              let price = Double(value) else {
                print("no price")
            return
        }
        guard let details = detailsField.text, details != "" else {
            print("no details")
            return
        }
        
        let item: Item!
        let picture = Image(context: context)
        picture.image = imageView.image
        
        if itemToEdit == nil {
            item = Item(context: context)
        } else {
            item = itemToEdit
        }
        
        item.toImage = picture
        item.title = title
        item.price = price
        item.details = details
        item.toStore = stores[storePicker.selectedRow(inComponent: 0)]
        
        ad.saveContext()
        
        navigationController?.popViewController(animated: true)
    }
    
    func loadItemData() {
        if let item = itemToEdit {
            titleField.text = item.title
            detailsField.text = item.details
            priceField.text = String(describing: item.price)
            imageView.image = item.toImage?.image as? UIImage
            
            if let storeIndex = stores.index(of: item.toStore!) {
                if storeIndex == 0 {
                    storePicker.selectRow(0, inComponent: 0, animated: false)
                } else {
                    storePicker.selectRow(storeIndex, inComponent: 0, animated: false)
                }
                
            }
        }
    }
    
    @IBAction func deleteItem(_ sender: UIBarButtonItem) {
        
        if itemToEdit != nil {
            context.delete(itemToEdit!)
            ad.saveContext()
            navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func imageBtnPressed(_ sender: Any) {
        present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            imageView.image = image
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }

}










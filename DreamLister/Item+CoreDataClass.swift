//
//  Item+CoreDataClass.swift
//  DreamLister
//
//  Created by Yaroslav Zhurbilo on 07.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import Foundation
import CoreData

@objc(Item)
public class Item: NSManagedObject {

    public override func awakeFromInsert() {
        self.created = NSDate()
    }
}

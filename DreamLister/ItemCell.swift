//
//  ItemCell.swift
//  DreamLister
//
//  Created by Yaroslav Zhurbilo on 07.06.17.
//  Copyright © 2017 Yaroslav Zhurbilo. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {
    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemTitle: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemDetails: UILabel!
    
    func configureCell(item: Item) {
        
        itemTitle.text   = item.title
        itemPrice.text   = "$\(item.price)"
        itemDetails.text = item.details
        itemImage.image  = item.toImage?.image as? UIImage
        
    }

}
